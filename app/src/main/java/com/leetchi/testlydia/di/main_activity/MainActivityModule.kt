package com.leetchi.testlydia.di.main_activity

import com.leetchi.testlydia.data_layer.ContactRepositoryImpl
import com.leetchi.testlydia.data_layer.datasource.ContactApiDataSource
import com.leetchi.testlydia.data_layer.datasource.ContactLocalDataSource
import com.leetchi.testlydia.data_layer.mapper.ContactModelDataMapper
import com.leetchi.testlydia.domain_layer.ContactRepository
import com.leetchi.testlydia.retrofit.ApiClientBuilder
import com.leetchi.testlydia.retrofit.ContactApiDataSourceImpl
import com.leetchi.testlydia.retrofit.ContactService
import com.leetchi.testlydia.room.AppDatabase
import com.leetchi.testlydia.room.ContactDao
import com.leetchi.testlydia.room.ContactLocalDataSourceImpl
import com.leetchi.testlydia.utils.NetworkUtils
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    @MainActivityScope
    fun provideContactService(): ContactService = ApiClientBuilder.build(ContactService::class.java)

    @Provides
    @MainActivityScope
    fun provideContactDao(
        appDatabase: AppDatabase
    ): ContactDao = appDatabase.contactDao()

    @Provides
    @MainActivityScope
    fun provideContactApiDataSource(
        contactService: ContactService
    ): ContactApiDataSource = ContactApiDataSourceImpl(
        contactService
    )

    @Provides
    @MainActivityScope
    fun provideContactLocalDataSource(
        contactDao: ContactDao
    ): ContactLocalDataSource = ContactLocalDataSourceImpl(
        contactDao
    )

    @Provides
    @MainActivityScope
    fun provideContactRepository(
        networkUtils: NetworkUtils,
        contactApiDataSource: ContactApiDataSource,
        contactLocalDataSource: ContactLocalDataSource,
        contactModelDataMapper: ContactModelDataMapper
    ): ContactRepository = ContactRepositoryImpl(
        networkUtils,
        contactApiDataSource,
        contactLocalDataSource,
        contactModelDataMapper
    )
}