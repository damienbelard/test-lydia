package com.leetchi.testlydia.di.application

import android.content.Context
import androidx.room.Room
import com.leetchi.testlydia.room.AppDatabase
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {
    @Provides
    @ApplicationScope
    fun provideRoomDatabase(
        applicationContext: Context
    ): AppDatabase {
        return Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "database"
        ).fallbackToDestructiveMigration().build()
    }
}