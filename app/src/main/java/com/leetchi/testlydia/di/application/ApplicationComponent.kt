package com.leetchi.testlydia.di.application

import android.content.Context
import com.leetchi.testlydia.room.AppDatabase
import dagger.BindsInstance
import dagger.Component

@ApplicationScope
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun getRoomDatabase(): AppDatabase

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(appContext: Context): Builder

        fun build(): ApplicationComponent
    }
}