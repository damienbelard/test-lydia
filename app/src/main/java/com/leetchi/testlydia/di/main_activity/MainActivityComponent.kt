package com.leetchi.testlydia.di.main_activity

import android.content.Context
import androidx.navigation.NavController
import com.leetchi.testlydia.MainActivity
import com.leetchi.testlydia.di.application.ApplicationComponent
import com.leetchi.testlydia.presentation_layer.details.DetailsFragment
import com.leetchi.testlydia.presentation_layer.home.HomeFragment
import dagger.BindsInstance
import dagger.Component

@Component(modules = [MainActivityModule::class], dependencies = [ApplicationComponent::class])
@MainActivityScope
interface MainActivityComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(homeFragment: HomeFragment)
    fun inject(detailsFragment: DetailsFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(context: Context): Builder

        fun appComponent(appComponent: ApplicationComponent): Builder

        @BindsInstance
        fun navController(navController: NavController): Builder

        //@BindsInstance
        //fun navController(navController: NavController): Builder

        fun build(): MainActivityComponent
    }
}