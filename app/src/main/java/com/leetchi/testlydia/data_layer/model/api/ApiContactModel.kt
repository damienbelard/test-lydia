package com.leetchi.testlydia.data_layer.model.api

import com.google.gson.annotations.SerializedName

data class NetworkContactModel(
    @SerializedName("results")
    val results: List<ApiResultsItem>?,
    @SerializedName("info")
    val apiInfo: ApiInfo
)