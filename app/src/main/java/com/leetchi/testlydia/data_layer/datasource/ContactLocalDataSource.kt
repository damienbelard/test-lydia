package com.leetchi.testlydia.data_layer.datasource

import com.leetchi.testlydia.data_layer.model.db.DbContactModel
import kotlinx.coroutines.flow.Flow

interface ContactLocalDataSource {
    suspend fun getContacts(page: Int): List<DbContactModel>
    fun saveContacts(contacts: List<DbContactModel>)
    fun deleteContacts()
    fun getContact(primaryKey: String): Flow<List<DbContactModel>>
}