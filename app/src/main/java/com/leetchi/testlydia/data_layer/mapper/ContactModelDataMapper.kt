package com.leetchi.testlydia.data_layer.mapper

import com.leetchi.testlydia.data_layer.model.api.ApiResultsItem
import com.leetchi.testlydia.data_layer.model.api.NetworkContactModel
import com.leetchi.testlydia.data_layer.model.db.DbContactModel
import com.leetchi.testlydia.di.main_activity.MainActivityScope
import com.leetchi.testlydia.domain_layer.model.*
import javax.inject.Inject

@MainActivityScope
class ContactModelDataMapper @Inject constructor() {
    fun mapNetworkContactToContactModelList(networkContactModel: NetworkContactModel): List<ContactModel> {
        return networkContactModel.results?.map {
            mapNetworkContactListToContactModel(it)
        } ?: emptyList()
    }

    fun mapDbContactListToContactModelList(dbContactModels: List<DbContactModel>): List<ContactModel> {
        return dbContactModels.map {
            ContactModel(
                if (it.gender == "male") Gender.MALE else Gender.FEMALE,
                it.firstname,
                it.lastname,
                Address(
                    it.street,
                    it.city,
                    it.state,
                    it.postcode
                ),
                it.email,
                Phone(
                    it.phone,
                    it.cellphone
                ),
                Picture(
                    it.largePictureUrl,
                    it.mediumPictureUrl,
                    it.smallPictureUrl
                )
            )
        }
    }

    fun mapNetworkContactListToDbContactList(networkContactModel: NetworkContactModel): List<DbContactModel> {
        return networkContactModel.results?.map {
            mapNetworkContactToDbContactModel(it, networkContactModel.apiInfo.page)
        } ?: emptyList()
    }

    private fun mapNetworkContactListToContactModel(apiResultsItem: ApiResultsItem): ContactModel {
        return ContactModel(
            if (apiResultsItem.gender == "male") Gender.MALE else Gender.FEMALE,
            apiResultsItem.apiName.first,
            apiResultsItem.apiName.last,
            Address(
                apiResultsItem.apiLocation.street,
                apiResultsItem.apiLocation.city,
                apiResultsItem.apiLocation.state,
                apiResultsItem.apiLocation.postcode
            ),
            apiResultsItem.email,
            Phone(
                apiResultsItem.phone,
                apiResultsItem.cell
            ),
            Picture(
                apiResultsItem.apiPicture.large,
                apiResultsItem.apiPicture.medium,
                apiResultsItem.apiPicture.thumbnail
            )
        )
    }

    private fun mapNetworkContactToDbContactModel(
        apiResultsItem: ApiResultsItem,
        page: Int
    ): DbContactModel {
        return DbContactModel(
            apiResultsItem.gender,
            apiResultsItem.apiName.first,
            apiResultsItem.apiName.last,
            apiResultsItem.apiLocation.street,
            apiResultsItem.apiLocation.city,
            apiResultsItem.apiLocation.state,
            apiResultsItem.apiLocation.postcode,
            apiResultsItem.email,
            apiResultsItem.phone,
            apiResultsItem.cell,
            apiResultsItem.apiPicture.large,
            apiResultsItem.apiPicture.medium,
            apiResultsItem.apiPicture.thumbnail,
            page
        )
    }
}