package com.leetchi.testlydia.data_layer.model.api

import com.google.gson.annotations.SerializedName

data class ApiLogin(
    @SerializedName("sha1")
    val sha1: String = "",
    @SerializedName("password")
    val password: String = "",
    @SerializedName("salt")
    val salt: String = "",
    @SerializedName("sha256")
    val sha256: String = "",
    @SerializedName("username")
    val username: String = "",
    @SerializedName("md5")
    val md: String = ""
)