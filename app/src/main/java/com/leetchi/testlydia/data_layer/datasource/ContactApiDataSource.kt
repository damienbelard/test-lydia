package com.leetchi.testlydia.data_layer.datasource

import com.leetchi.testlydia.data_layer.model.api.NetworkContactModel

interface ContactApiDataSource {
    suspend fun getContacts(page: Int): NetworkContactModel
}