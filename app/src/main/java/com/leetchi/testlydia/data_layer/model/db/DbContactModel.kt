package com.leetchi.testlydia.data_layer.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DbContactModel(
    val gender: String,
    val firstname: String,
    val lastname: String,
    val street: String,
    val city: String,
    val state: String,
    val postcode: String,
    val email: String,
    val phone: String,
    @PrimaryKey
    val cellphone: String,
    val largePictureUrl: String,
    val mediumPictureUrl: String,
    val smallPictureUrl: String,
    val page: Int
)