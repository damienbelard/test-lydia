package com.leetchi.testlydia.data_layer.model.api

import com.google.gson.annotations.SerializedName

data class ApiName(
    @SerializedName("last")
    val last: String = "",
    @SerializedName("title")
    val title: String = "",
    @SerializedName("first")
    val first: String = ""
)