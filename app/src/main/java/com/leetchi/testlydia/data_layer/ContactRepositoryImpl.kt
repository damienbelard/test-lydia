package com.leetchi.testlydia.data_layer

import com.leetchi.testlydia.data_layer.datasource.ContactApiDataSource
import com.leetchi.testlydia.data_layer.datasource.ContactLocalDataSource
import com.leetchi.testlydia.data_layer.mapper.ContactModelDataMapper
import com.leetchi.testlydia.domain_layer.ContactRepository
import com.leetchi.testlydia.domain_layer.model.ContactModel
import com.leetchi.testlydia.utils.NetworkUtils
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ContactRepositoryImpl(
    private val networkUtils: NetworkUtils,
    private val contactApiDataSource: ContactApiDataSource,
    private val contactLocalDataSource: ContactLocalDataSource,
    private val contactModelDataMapper: ContactModelDataMapper
) : ContactRepository {

    override suspend fun getContacts(page: Int): List<ContactModel> {
        return if (networkUtils.isOnline()) {
            contactApiDataSource.getContacts(page).let {
                if (page == 1) {
                    contactLocalDataSource.deleteContacts()
                }
                contactLocalDataSource.saveContacts(
                    contactModelDataMapper.mapNetworkContactListToDbContactList(it)
                )
                contactModelDataMapper.mapNetworkContactToContactModelList(it)
            }
        } else contactModelDataMapper.mapDbContactListToContactModelList(
            contactLocalDataSource.getContacts(page)
        )
    }

    override fun getContact(primaryKey: String): Flow<ContactModel> =
        contactLocalDataSource.getContact(primaryKey).map {
            contactModelDataMapper.mapDbContactListToContactModelList(it).first()
        }
}