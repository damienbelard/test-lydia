package com.leetchi.testlydia.data_layer.model.api

import com.google.gson.annotations.SerializedName

data class ApiId(
    @SerializedName("name")
    val name: String = "",
    @SerializedName("value")
    val value: String = ""
)