package com.leetchi.testlydia.data_layer.model.api

import com.google.gson.annotations.SerializedName

data class ApiResultsItem(
    @SerializedName("nat")
    val nat: String = "",
    @SerializedName("gender")
    val gender: String = "",
    @SerializedName("phone")
    val phone: String = "",
    @SerializedName("dob")
    val dob: Int = 0,
    @SerializedName("name")
    val apiName: ApiName,
    @SerializedName("registered")
    val registered: Int = 0,
    @SerializedName("location")
    val apiLocation: ApiLocation,
    @SerializedName("id")
    val apiId: ApiId,
    @SerializedName("login")
    val login: ApiLogin,
    @SerializedName("cell")
    val cell: String = "",
    @SerializedName("email")
    val email: String = "",
    @SerializedName("picture")
    val apiPicture: ApiPicture
)