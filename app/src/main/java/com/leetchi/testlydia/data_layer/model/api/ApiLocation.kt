package com.leetchi.testlydia.data_layer.model.api

import com.google.gson.annotations.SerializedName

data class ApiLocation(
    @SerializedName("city")
    val city: String = "",
    @SerializedName("street")
    val street: String = "",
    @SerializedName("postcode")
    val postcode: String = "",
    @SerializedName("state")
    val state: String = ""
)