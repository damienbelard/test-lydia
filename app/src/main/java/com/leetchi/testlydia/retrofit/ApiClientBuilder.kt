package com.leetchi.testlydia.retrofit

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClientBuilder {
    private const val API_BASE_URL = "https://randomuser.me/api/1.0/"

    private val retrofit = Retrofit.Builder()
        .baseUrl(API_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()

    fun <T> build(apiClass: Class<T>): T {
        return retrofit.create(apiClass)
    }
}