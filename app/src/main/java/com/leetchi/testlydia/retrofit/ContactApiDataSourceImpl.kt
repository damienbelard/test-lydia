package com.leetchi.testlydia.retrofit

import com.leetchi.testlydia.data_layer.datasource.ContactApiDataSource
import com.leetchi.testlydia.data_layer.model.api.NetworkContactModel

class ContactApiDataSourceImpl(
    private val contactService: ContactService
) : ContactApiDataSource {
    override suspend fun getContacts(page: Int): NetworkContactModel =
        contactService.getContactsWithPage(page)
}