package com.leetchi.testlydia.retrofit

import com.leetchi.testlydia.data_layer.model.api.NetworkContactModel
import retrofit2.http.GET
import retrofit2.http.Query

interface ContactService {
    @GET("?seed=lydia&results=10")
    suspend fun getContactsWithPage(@Query("page") page: Int): NetworkContactModel
}