package com.leetchi.testlydia

import android.app.Application
import com.leetchi.testlydia.di.application.ApplicationComponent
import com.leetchi.testlydia.di.application.DaggerApplicationComponent
import timber.log.Timber

class TestLydiaApplication : Application() {
    lateinit var appComponent: ApplicationComponent


    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        appComponent = DaggerApplicationComponent.builder()
            .context(this)
            .build()
    }
}