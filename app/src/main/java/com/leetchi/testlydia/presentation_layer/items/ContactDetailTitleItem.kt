package com.leetchi.testlydia.presentation_layer.items

import com.leetchi.testlydia.R
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_contact_detail_title.*

class ContactDetailTitleItem(
    private val title: String
) : Item() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.contactDetailTitleItem.text = title
    }

    override fun getLayout(): Int = R.layout.item_contact_detail_title
}