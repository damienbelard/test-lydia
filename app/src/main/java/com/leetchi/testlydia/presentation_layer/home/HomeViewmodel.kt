package com.leetchi.testlydia.presentation_layer.home

import androidx.lifecycle.*
import androidx.navigation.NavController
import com.leetchi.testlydia.domain_layer.ContactUseCase
import com.leetchi.testlydia.presentation_layer.items.ContactListItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*
import javax.inject.Inject

@ExperimentalStdlibApi
class HomeViewmodel(
    private val contactUseCase: ContactUseCase,
    private val navController: NavController
) : ViewModel() {
    /* Factory for creating FeatureViewModel instances */
    class Factory @Inject constructor(
        private val contactUseCase: ContactUseCase,
        private val navController: NavController
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return HomeViewmodel(
                contactUseCase,
                navController
            ) as T
        }
    }

    private val _state: MutableLiveData<HomeState> = MutableLiveData()
    val state: LiveData<HomeState>
        get() = _state

    private var currentPage: Int = 1
    private val items: MutableList<ContactListItem> = mutableListOf()

    init {
        _state.value = HomeState()
        loadPage(1)
    }

    fun onReload() {
        currentPage = 1
        loadPage(1)
    }

    fun onEndPage() {
        currentPage++
        loadPage(currentPage)
        Timber.e("currentPage: $currentPage")
    }

    private fun loadPage(page: Int) {
        viewModelScope.launch(Dispatchers.Main) {
            val contacts = withContext(Dispatchers.IO) {
                return@withContext contactUseCase.getContacts(page)
            }
            val items = contacts.map {
                ContactListItem(
                    it.phone.cellPhone,
                    it.picture.largePictureUrl,
                    it.firstname.capitalize(Locale.getDefault()) + " " + it.lastname.capitalize(
                        Locale.getDefault()
                    ),
                    it.email
                )
            }
            this@HomeViewmodel.items.addAll(items)
            _state.value = _state.value!!.copy(
                isLoading = false,
                contacts = this@HomeViewmodel.items
            )
        }
    }

    fun onItemClicked(primaryKey: String) {
        val action = HomeFragmentDirections.actionHomeFragmentToDetailsFragment(primaryKey)
        navController.navigate(action)
    }
}