package com.leetchi.testlydia.presentation_layer.items

import com.leetchi.testlydia.R
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_contact_detail_email.*

class ContactDetailEmailItem(
    val email: String
) : Item() {

    override fun getLayout(): Int = R.layout.item_contact_detail_email

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.contactEmailItemTextView.text = email
    }
}