package com.leetchi.testlydia.presentation_layer.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.leetchi.testlydia.MainActivity
import com.leetchi.testlydia.R
import com.leetchi.testlydia.listener.InfiniteScrollListener
import com.leetchi.testlydia.presentation_layer.items.ContactListItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class HomeFragment : Fragment(R.layout.fragment_home) {
    @ExperimentalStdlibApi
    @ExperimentalCoroutinesApi
    @Inject
    lateinit var vmFactory: HomeViewmodel.Factory

    @ExperimentalStdlibApi
    @ExperimentalCoroutinesApi
    private lateinit var model: HomeViewmodel

    private lateinit var groupAdapter: GroupAdapter<GroupieViewHolder>

    @ExperimentalCoroutinesApi
    @ExperimentalStdlibApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as MainActivity).mainActivityComponent.inject(this)

        groupAdapter = GroupAdapter()
        groupAdapter.setOnItemClickListener { item, _ ->
            val primaryKey = (item as ContactListItem).primaryKey
            model.onItemClicked(primaryKey)
        }

        val layoutManager = LinearLayoutManager(context)
        homeFragmentRecyclerView.apply {
            adapter = groupAdapter
            this.layoutManager = layoutManager
            addOnScrollListener(object :
                InfiniteScrollListener(layoutManager) {
                override fun onLoadMore(current_page: Int) {
                    model.onEndPage()
                }
            })
        }

        model = ViewModelProvider(this, vmFactory)[HomeViewmodel::class.java]
        model.state.observe(viewLifecycleOwner, Observer { state ->
            renderState(state)
        })

        homeSwipeRefresh.setOnRefreshListener {
            model.onReload()
        }

        requireActivity().window.apply {
            val color = resources.getColor(R.color.colorPrimary)
            if (statusBarColor != color) statusBarColor = color
        }
    }

    private fun renderState(state: HomeState) {
        homeSwipeRefresh.isRefreshing = state.isLoading
        if (state.contacts.isNotEmpty()) {
            homeFragmentRecyclerView.visibility = View.VISIBLE
            homeFragmentNoDataTextview.visibility = View.GONE
            groupAdapter.apply {
                clear()
                addAll(state.contacts)
            }
        } else if (!state.isLoading) {
            homeFragmentRecyclerView.visibility = View.GONE
            homeFragmentNoDataTextview.visibility = View.VISIBLE
        }
    }
}