package com.leetchi.testlydia.presentation_layer.details

import android.content.Context
import androidx.lifecycle.*
import com.leetchi.testlydia.R
import com.leetchi.testlydia.domain_layer.ContactUseCase
import com.leetchi.testlydia.presentation_layer.items.ContacDetailLocationItem
import com.leetchi.testlydia.presentation_layer.items.ContactDetailEmailItem
import com.leetchi.testlydia.presentation_layer.items.ContactDetailPhoneItem
import com.leetchi.testlydia.presentation_layer.items.ContactDetailTitleItem
import com.leetchi.testlydia.utils.EmailUtils
import com.leetchi.testlydia.utils.MapUtils
import com.leetchi.testlydia.utils.PhoneUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class DetailsViewmodel(
    private val contactUseCase: ContactUseCase,
    private val phoneUtils: PhoneUtils,
    private val emailUtils: EmailUtils,
    private val mapUtils: MapUtils,
    private val context: Context
) : ViewModel() {
    /* Factory for creating FeatureViewModel instances */
    class Factory @Inject constructor(
        private val contactUseCase: ContactUseCase,
        private val phoneUtils: PhoneUtils,
        private val emailUtils: EmailUtils,
        private val mapUtils: MapUtils,
        private val context: Context
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return DetailsViewmodel(
                contactUseCase,
                phoneUtils,
                emailUtils,
                mapUtils,
                context
            ) as T
        }
    }

    private val _state: MutableLiveData<DetailsState> = MutableLiveData()
    val state: LiveData<DetailsState>
        get() = _state

    init {
        _state.value = DetailsState()
    }

    @ExperimentalStdlibApi
    @ExperimentalCoroutinesApi
    fun fetchContact(primaryKey: String) {
        _state.value = _state.value!!.copy(
            isLoading = true
        )
        viewModelScope.launch(Dispatchers.Main) {
            contactUseCase.getContact(primaryKey)
                .flowOn(Dispatchers.IO)
                .collect {
                    _state.value = _state.value!!.copy(
                        isLoading = false,
                        title = it.firstname.capitalize(Locale.getDefault()) + " " + it.lastname.capitalize(
                            Locale.getDefault()
                        ),
                        imageUrl = it.picture.largePictureUrl,
                        items = listOf(
                            ContactDetailTitleItem(
                                context.resources.getString(R.string.item_title_cellphone)
                            ),
                            ContactDetailPhoneItem(
                                it.phone.cellPhone
                            ),
                            ContactDetailTitleItem(
                                context.resources.getString(R.string.item_title_groundline)
                            ),
                            ContactDetailPhoneItem(
                                it.phone.groundLine
                            ),
                            ContactDetailTitleItem(
                                context.resources.getString(R.string.item_title_email)
                            ),
                            ContactDetailEmailItem(
                                it.email
                            ),
                            ContactDetailTitleItem(
                                context.resources.getString(R.string.item_title_address)
                            ),
                            ContacDetailLocationItem(
                                it.address.street
                                        + "\n"
                                        + it.address.city.capitalize(
                                    Locale.getDefault()
                                )
                                        + "\n"
                                        + it.address.postcode
                            )
                        )
                    )
                }
        }
    }

    fun onPhoneNumberClicked(phonenumber: String) {
        phoneUtils.makePhoneCall(phonenumber)
    }

    fun onEmailClicked(email: String) {
        emailUtils.sendEmail(email)
    }

    fun onLocationClicked(address: String) {
        mapUtils.openLocation(address)
    }
}