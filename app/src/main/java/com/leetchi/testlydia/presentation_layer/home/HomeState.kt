package com.leetchi.testlydia.presentation_layer.home

import com.leetchi.testlydia.presentation_layer.items.ContactListItem

data class HomeState(
    val isLoading: Boolean = true,
    val contacts: List<ContactListItem> = emptyList()
)