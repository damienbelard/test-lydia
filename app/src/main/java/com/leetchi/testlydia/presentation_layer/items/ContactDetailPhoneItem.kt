package com.leetchi.testlydia.presentation_layer.items

import com.leetchi.testlydia.R
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_contact_detail_phone.*

class ContactDetailPhoneItem(
    val phone: String
) : Item() {

    override fun getLayout(): Int = R.layout.item_contact_detail_phone

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.contactItemTextViewName.text = phone
    }
}