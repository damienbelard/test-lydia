package com.leetchi.testlydia.presentation_layer.items

import com.leetchi.testlydia.R
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_contact_details_location.*

class ContacDetailLocationItem(
    val address: String
) : Item() {

    override fun getLayout(): Int = R.layout.item_contact_details_location

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.contactLocationItemTextView.text = address
    }
}