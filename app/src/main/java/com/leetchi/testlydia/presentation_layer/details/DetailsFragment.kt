package com.leetchi.testlydia.presentation_layer.details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import coil.api.load
import com.leetchi.testlydia.MainActivity
import com.leetchi.testlydia.R
import com.leetchi.testlydia.presentation_layer.items.ContacDetailLocationItem
import com.leetchi.testlydia.presentation_layer.items.ContactDetailEmailItem
import com.leetchi.testlydia.presentation_layer.items.ContactDetailPhoneItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class DetailsFragment : Fragment(R.layout.fragment_details) {

    @ExperimentalStdlibApi
    @ExperimentalCoroutinesApi
    @Inject
    lateinit var vmFactory: DetailsViewmodel.Factory

    @ExperimentalStdlibApi
    @ExperimentalCoroutinesApi
    private lateinit var model: DetailsViewmodel

    lateinit var primaryKey: String

    private lateinit var groupAdapter: GroupAdapter<GroupieViewHolder>

    @ExperimentalCoroutinesApi
    @ExperimentalStdlibApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        primaryKey = DetailsFragmentArgs.fromBundle(requireArguments()).primaryKey

        (requireActivity() as MainActivity).mainActivityComponent.inject(this)

        model = ViewModelProvider(this, vmFactory)[DetailsViewmodel::class.java]
        model.state.observe(viewLifecycleOwner, Observer { state ->
            renderState(state)
        })

        groupAdapter = GroupAdapter()
        groupAdapter.setOnItemClickListener { item, _ ->
            when (item) {
                is ContactDetailPhoneItem -> model.onPhoneNumberClicked(item.phone)
                is ContactDetailEmailItem -> model.onEmailClicked(item.email)
                is ContacDetailLocationItem -> model.onLocationClicked(item.address)
            }
        }
        detailsContactInfo.apply {
            adapter = groupAdapter
            this.layoutManager = LinearLayoutManager(requireContext())
        }

        detailsSwipeRefresh.setOnRefreshListener {
            model.fetchContact(primaryKey)
        }

        model.fetchContact(primaryKey)

        requireActivity().window.apply {
            val color = resources.getColor(android.R.color.transparent)
            statusBarColor = color
        }
    }

    @ExperimentalStdlibApi
    private fun renderState(state: DetailsState) {
        detailsSwipeRefresh.isRefreshing = state.isLoading
        collapsingToolbar.title = state.title
        detailsImage.load(state.imageUrl)
        groupAdapter.apply {
            clear()
            addAll(state.items)
        }
    }
}