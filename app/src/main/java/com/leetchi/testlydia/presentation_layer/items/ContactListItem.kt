package com.leetchi.testlydia.presentation_layer.items

import coil.api.load
import com.leetchi.testlydia.R
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_contact_list.*

class ContactListItem(
    val primaryKey: String,
    private val avatarUrl: String,
    private val contactName: String,
    private val contactEmail: String
) : Item() {
    override fun getLayout(): Int = R.layout.item_contact_list

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.contactItemImageView.load(avatarUrl)
        viewHolder.contactItemTextViewName.text = contactName
        viewHolder.contactItemTextViewEmail.text = contactEmail
    }
}