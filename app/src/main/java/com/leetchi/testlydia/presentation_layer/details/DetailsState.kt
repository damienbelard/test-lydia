package com.leetchi.testlydia.presentation_layer.details

data class DetailsState(
    val isLoading: Boolean = false,
    val title: String = "",
    val imageUrl: String = "",
    val items: List<com.xwray.groupie.kotlinandroidextensions.Item> = emptyList()
)