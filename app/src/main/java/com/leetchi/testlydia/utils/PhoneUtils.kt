package com.leetchi.testlydia.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.content.ContextCompat.startActivity
import com.leetchi.testlydia.di.main_activity.MainActivityScope
import javax.inject.Inject

@MainActivityScope
class PhoneUtils @Inject constructor(
    private val context: Context
) {
    fun makePhoneCall(phoneNumber: String) {
        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNumber"))
        startActivity(context, intent, null)
    }
}