package com.leetchi.testlydia.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import javax.inject.Inject


class MapUtils @Inject constructor(
    private val context: Context
) {
    fun openLocation(address: String) {
        val uri =
            "geo:0,0?q=$address"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        context.startActivity(Intent.createChooser(intent, "Select an application"))
    }
}