package com.leetchi.testlydia.utils

import android.content.Context
import android.net.ConnectivityManager
import com.leetchi.testlydia.di.main_activity.MainActivityScope
import javax.inject.Inject

@MainActivityScope
class NetworkUtils @Inject constructor(
    private val context: Context
) {
    fun isOnline(): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo?.isConnected == true
    }
}
