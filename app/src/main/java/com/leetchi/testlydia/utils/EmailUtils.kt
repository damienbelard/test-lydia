package com.leetchi.testlydia.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import javax.inject.Inject

class EmailUtils @Inject constructor(
    private val context: Context
) {
    fun sendEmail(address: String) {
        val mailTo = "mailto:$address"
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse(mailTo)

        context.startActivity(intent)
    }
}