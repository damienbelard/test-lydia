package com.leetchi.testlydia

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.leetchi.testlydia.di.main_activity.DaggerMainActivityComponent
import com.leetchi.testlydia.di.main_activity.MainActivityComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi

class MainActivity : AppCompatActivity() {
    lateinit var mainActivityComponent: MainActivityComponent

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainActivityComponent = DaggerMainActivityComponent.builder()
            .context(this)
            .navController(findNavController(R.id.root))
            .appComponent((application as TestLydiaApplication).appComponent)
            .build()
    }
}