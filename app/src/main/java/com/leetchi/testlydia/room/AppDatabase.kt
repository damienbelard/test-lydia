package com.leetchi.testlydia.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.leetchi.testlydia.data_layer.model.db.DbContactModel

@Database(
    entities = [DbContactModel::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun contactDao(): ContactDao
}