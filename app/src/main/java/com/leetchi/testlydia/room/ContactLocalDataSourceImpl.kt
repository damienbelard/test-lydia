package com.leetchi.testlydia.room

import com.leetchi.testlydia.data_layer.datasource.ContactLocalDataSource
import com.leetchi.testlydia.data_layer.model.db.DbContactModel
import kotlinx.coroutines.flow.Flow

class ContactLocalDataSourceImpl(
    private val contactDao: ContactDao
) : ContactLocalDataSource {
    override suspend fun getContacts(page: Int): List<DbContactModel> =
        contactDao.getAllContactsFromPage(page)

    override fun saveContacts(contacts: List<DbContactModel>) = contactDao.insertContacts(contacts)
    override fun deleteContacts() = contactDao.deleteContacts()
    override fun getContact(primaryKey: String): Flow<List<DbContactModel>> =
        contactDao.getContact(primaryKey)
}