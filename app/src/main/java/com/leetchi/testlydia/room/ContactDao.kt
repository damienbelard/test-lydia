package com.leetchi.testlydia.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.leetchi.testlydia.data_layer.model.db.DbContactModel
import kotlinx.coroutines.flow.Flow

@Dao
interface ContactDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertContacts(contacts: List<DbContactModel>)

    @Query("DELETE FROM DbContactModel")
    fun deleteContacts()

    @Query("SELECT * FROM DbContactModel WHERE page IS :selectedPage")
    suspend fun getAllContactsFromPage(selectedPage: Int): List<DbContactModel>

    @Query("SELECT * FROM DbContactModel WHERE cellphone IS :primaryKey")
    fun getContact(primaryKey: String): Flow<List<DbContactModel>>
}