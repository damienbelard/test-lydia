package com.leetchi.testlydia.domain_layer.model

data class ContactModel(
    val gender: Gender,
    val firstname: String,
    val lastname: String,
    val address: Address,
    val email: String,
    val phone: Phone,
    val picture: Picture
)