package com.leetchi.testlydia.domain_layer.model

enum class Gender {
    MALE,
    FEMALE
}