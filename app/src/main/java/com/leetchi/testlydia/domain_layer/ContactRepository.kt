package com.leetchi.testlydia.domain_layer

import com.leetchi.testlydia.domain_layer.model.ContactModel
import kotlinx.coroutines.flow.Flow

interface ContactRepository {
    suspend fun getContacts(page: Int): List<ContactModel>
    fun getContact(primaryKey: String): Flow<ContactModel>
}