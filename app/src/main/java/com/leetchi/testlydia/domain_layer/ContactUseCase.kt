package com.leetchi.testlydia.domain_layer

import com.leetchi.testlydia.domain_layer.model.ContactModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ContactUseCase @Inject constructor(
    private val repository: ContactRepository
) {
    suspend fun getContacts(page: Int): List<ContactModel> = repository.getContacts(page)
    fun getContact(primaryKey: String): Flow<ContactModel> = repository.getContact(primaryKey)
}