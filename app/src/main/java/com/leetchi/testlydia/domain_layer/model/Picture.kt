package com.leetchi.testlydia.domain_layer.model

data class Picture(
    val largePictureUrl: String,
    val mediumPictreUrl: String,
    val smallPictureUrl: String
)