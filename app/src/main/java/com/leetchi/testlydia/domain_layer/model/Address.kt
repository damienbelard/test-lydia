package com.leetchi.testlydia.domain_layer.model

data class Address(
    val street: String,
    val city: String,
    val state: String,
    val postcode: String
)