package com.leetchi.testlydia.domain_layer.model

data class Phone(
    val groundLine: String,
    val cellPhone: String
)